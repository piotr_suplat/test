<?php
//class that handles all datatcalls for the stats dashboard

require_once('dbconfig.php'); //include database config file

class Stats
{

	private $conn;
    
    //create new connection
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
 
    //prepare query
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}


    /**
    get top ten slow requets of all time
    */
	public function getTopSLow()
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT * FROM biztalk_stats_extract ORDER BY exec_time DESC LIMIT 0,10 ");
            $stmt->execute();
			$data=$stmt->fetchAll();
			if($stmt->rowCount() > 0)
			{
				return $data;
			}
      else
      {
        return 'No data available';
      }
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

    /**
    getnumber of call made per type
    */
    public function getTypeSplit()
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT count(*) as counter, type FROM biztalk_stats_extract GROUP BY type");
      $stmt->execute();
			$data=$stmt->fetchAll();
      $new_data = array(['Call type', 'Number of calls']);
			if($stmt->rowCount() > 0)
			{
        foreach($data as $d)
        {
          $new_data[] = array($d['type'],intval($d['counter']));
        }
        return $new_data;
			}
      else
      {
        return 'No data available';
      }
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

    /**
    getnumber of call made per method
    */
    public function getMethodSplit()
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT count(*) as counter, method FROM biztalk_stats_extract GROUP BY method");
      $stmt->execute();
			$data=$stmt->fetchAll();
      $new_data = array(['Method', 'Number of calls']);
			if($stmt->rowCount() > 0)
			{
        foreach($data as $d)
        {
          $new_data[] = array($d['method'],intval($d['counter']));
        }
        return $new_data;
			}
      else
      {
        return 'No data available';
      }
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

    /**
    get list of distinct values for each column defined by $column_name variable. The results are being used by the custom query builder form
    */
    public function getDistinctColumn($column_name)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT DISTINCT $column_name FROM biztalk_stats_extract ORDER BY $column_name ASC;");
      $stmt->execute();
			$data=$stmt->fetchAll(PDO::FETCH_COLUMN);
			if($stmt->rowCount() > 0)
			{
        return $data;
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
    
    /**
    build a custome query based on the values supplied by the user
    */
    public function runCustomQuery($method,$type,$from,$to,$time_rule,$time,$controller, $action)
	{

    // start building condtions
    $conditions = array();
    $parameters = array();
    $where = "";

    //if there is a condition for method
    if($method != 'all')
    {
      $conditions[] = "method = :method";
      $parameters[":method"] = $method;
    }

    //if there is a condition for type
    if($type != 'all')
    {
      $conditions[] = "type = :type";
      $parameters[":type"] = $type;
    }

    //if there is a condition for timeframe
    if($from != '' && $to !='')
    {
      //reformat dates
      $boom_from = explode('/',$from);
      $new_from = $boom_from[2].'-'.$boom_from[1].'-'.$boom_from[0].' 00:00:00'; //rebuild and add time

      $boom_to = explode('/',$to);
      $new_to = $boom_to[2].'-'.$boom_to[1].'-'.$boom_to[0].' 23:59:59'; //rebuild and add time

      $conditions[] = "request_time BETWEEN :start_date AND :end_date";
      $parameters[":start_date"] = $new_from;
      $parameters[":end_date"] = $new_to;
    }

    //if there is a condition for execution type
    if($time != '')
    {

      $conditions[] = "exec_time $time_rule :exec_time";
      $parameters[":exec_time"] = $time;
    }

    //if there is a condition for controller
    if($controller != 'all')
    {
      $conditions[] = "sourcecontroller = :controller";
      $parameters[":controller"] = $controller;
    }

    //if there is a condition for action
    if($action != 'all')
    {
      $conditions[] = "sourceaction = :action";
      $parameters[":action"] = $action;
    }

    if (count($conditions) > 0)
    {
        $where = implode(' AND ', $conditions);
    }

    $query = "SELECT * FROM biztalk_stats_extract " . ($where != "" ? " WHERE $where" : "");


    try
		{
      if (empty($parameters)) //if parametres array is emtpy just execute the query
      {
          $stmt = $this->conn->prepare($query);
          $stmt->execute();
      }
      else //else execute query with the parameterised where condition
      {
          $stmt = $this->conn->prepare($query);
          $stmt->execute($parameters);

      }
      $data = $stmt->fetchAll(); //get all resulting data

      if($stmt->rowCount() > 0)
      {
        return $data;
      }
      else
      {
        return 'No data found';
      }

		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}





}
