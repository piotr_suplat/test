<?php

//include and instantiate the stats class
require_once("stats.php");
$stats = new Stats();

//call the method with all variable. variables were passed by ajax call
$data = $stats->runCustomQuery($_POST['fld_method'],$_POST['fld_type'],$_POST['fld_date_from'],$_POST['fld_date_to'],$_POST['fld_execution_rule'],$_POST['fld_execution'],$_POST['fld_controller'], $_POST['fld_action']);
if(is_array($data)) //array means there is data
{
  //generate csv file and download link
  $csvData = '';
  foreach($data as $row)
  {
	$csvData .= $row['method'].",".$row['type'].",".$row['request_time'].",".$row['exec_time'].",".$row['sourcecontroller'].",".$row['sourceaction']. "\r\n";
  }
  $dir = 'temp/';
  $file = 'data.csv';
  $handle = fopen($dir . $file, 'w') or die('Error creating file:  ' . $dir . $file); //implicitly creates file
  fwrite($handle, $csvData); //write data to file
  echo '<a href="temp/' . $file . '" id="dnl_csv_data" style="position:absolute; top: 0; right:0; float:right;margin-bottom:10px" title="Download data file"><image src="assets/images/download-csv-icon.gif"></a>';
	
  //generate table
  echo '<table class="custom_query_table">';
	echo '<thead><tr><th>Method</th><th>Type</th><th>Requested</th><th>Execution time (ms)</th><th>Controller</th><th>Action</th></tr></thead>';
	echo '<tbody>';
	foreach($data as $row)
	{
		echo '<tr><td>'.$row['method'].'</td><td>'.$row['type'].'</td><td>'.$row['request_time'].'</td><td>'.$row['exec_time'].'</td><td>'.$row['sourcecontroller'].'</td><td>'.$row['sourceaction'].'</td></tr>';
	}
	echo '</tbody>';
	echo '</table>';
	
}
else //there is no data
{
	echo $data; //show no data message
}

?>
