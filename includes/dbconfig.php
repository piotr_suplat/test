<?php
/**
DB connection class, using PDO
*/
class Database
{
    //local connection details
    private $host = "localhost";
    private $db_name = "biztalk";
    private $username = "root";
    private $password = "Pa55w0rd";
    
    //hosting demo details
    //private $host = "mysql.hostinger.co.uk";
    //private $db_name = "u596837295_king";
    //private $username = "u596837295_king";
    //private $password = "Pa55w0rd";
    
    public $conn;

    public function dbConnection()
	{

	    $this->conn = null;
        try //try to establish the connection to DB
		{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
		catch(PDOException $exception) //if attempt to connect failed show error message
		{
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn; //return the connection object
    }
}
