<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stats</title>
<link rel="stylesheet" href="assets/css/style.css" type="text/css"  />
</head>
<body>
<h1>Statistical Dashboard</h1>
<div class="dashlet" style="width: 45%; height: 400px;">
<h3>Top 10 Slowest Requests</h3>
<?php
//load dashlet with the top ten slowest call
include 'dashlets/dash_top_ten_slow.php';
?>
</div>
<div class="dashlet" style="width: 45%; height: 400px; float:right">
<?php
//load dashlet with the pie charts for calls split for type and methods
include 'dashlets/dash_type_split.php';
?>
</div>
<div class="dashlet" style="width: 98%;margin-top:20px">
<h3>Custom report builder</h3>
<?php
//load dashlet for custom query builder
include 'dashlets/dash_custom_data.php';
?>
</div>

</body>
</html>
