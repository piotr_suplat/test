<?php
/**
this file will generate pie chart showing the split between types of calls an called methods. Google charting libray is being used to generate the charts 
*/

//load and instantiate the stats class
require_once("includes/stats.php");
$stats = new Stats();

$type_split = $stats->getTypeSplit(); //get split by type
$method_split = $stats->getMethodSplit(); //get split by method

if(is_array($type_split)) //array means there is data
{
  $type_data =  json_encode($type_split); 

?>
<div style="float:left;height:100%;width:50%"><h3>Number of call per type</h3><div id="type_split_chart"  style="height:80%;width:100%"></div></div>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- Generate split by type pie chart -->
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable(<?php echo $type_data; ?>);

        var options = {
          title: 'Number of calls per request type',
          chartArea: {width: '96%',height: '400'},
          pieSliceText: 'value'
        };

        var chart = new google.visualization.PieChart(document.getElementById('type_split_chart'));

        chart.draw(data, options);
      }
    </script>

<?php
}
else {
echo $type_split; //else the variable stores a text informing user there is no data so just show that message
}

if(is_array($method_split)) //array means there is data
{
  $method_data =  json_encode($method_split);

?>
<div style="float:left;height:100%;width:50%"><h3>Number of call per method</h3><div id="method_split_chart" style="height:80%;width:100%"></div></div>

<!-- Generate split by method pie chart -->
<script type="text/javascript">
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable(<?php echo $method_data; ?>);

        var options = {
          title: 'Number of calls per method',
          chartArea: {width: '96%',height: '400'},
          hAxis: {
            title: 'Number of calls',
            minValue: 0
          },
          vAxis: {
            title: 'Mehod'
          },
          pieSliceText: 'value'
        };

        var chart = new google.visualization.PieChart(document.getElementById('method_split_chart'));

        chart.draw(data, options);
      }
    </script>

<?php
}
else {
echo $method_split; //else the variable stores a text informing user there is no data so just show that message
}

 ?>
