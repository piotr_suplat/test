<?php
//load and instantiate the stats class
require_once("includes/stats.php");
$stats = new Stats();

$top_ten_slow = $stats->getTopSLow(); //call method
if(is_array($top_ten_slow)) //array means there is data
{
	echo '<table>';
	echo '<thead><tr><th>Method</th><th>Type</th><th>Requested</th><th>Execution time (ms)</th><th>Controller</th><th>Action</th></tr></thead>';
	echo '<tbody>';
	foreach($top_ten_slow as $tops)
	{
		echo '<tr><td>'.$tops['method'].'</td><td>'.$tops['type'].'</td><td>'.$tops['request_time'].'</td><td>'.$tops['exec_time'].'</td><td>'.$tops['sourcecontroller'].'</td><td>'.$tops['sourceaction'].'</td></tr>';
	}
	echo '</tbody>';
	echo '</table>';
}
else //there is no data
{
	echo $top_ten_slow;
}
