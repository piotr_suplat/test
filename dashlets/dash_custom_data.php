<?php
//load and instantiate the stats class
require_once("includes/stats.php");
$stats = new Stats();

//get all distinctive values from given columns for query building form
$list_methods = $stats->getDistinctColumn('method');
$list_types = $stats->getDistinctColumn('type');
$list_controllers = $stats->getDistinctColumn('sourcecontroller');
$list_actions = $stats->getDistinctColumn('sourceaction');
?>
<!-- using jquery with jquery UI to  get the calendar controlls and easy ajax calls -->
<script type="text/javascript" src="assets/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui-1.12.0/jquery-ui.js"></script>
<link rel="stylesheet" href="assets/js/jquery-ui-1.12.0/jquery-ui.css">

<form>
	<button class="btn_submit_query" type="button" id="submit_custom_query">Run <image src="assets/images/go.png" style="width:20px;height:20px"></button>
<table >
	<thead>
		<tr>
			<th>Method</th>
			<th>Type</th>
			<th colspan="2">Timeframe</th>
			<th>Execution Time</th>
			<th>Controller</th>
			<th>Action</th>
		</tr>
	</thead>
		<tr>
			<th>
				<select id="q_method">
					<option value="all">all</option>
					<?php
					foreach($list_methods as $value)
					{
						echo '<option value="'.$value.'">'.$value.'</option>';
					}
					?>
				</select>
			</th>
			<th>
				<select id="q_type">
					<option value="all">all</option>
					<?php
					foreach($list_types as $value)
					{
						echo '<option value="'.$value.'">'.$value.'</option>';
					}
					?>
				</select>
			</th>
			<th>
				From:
				<input type="datetime" id="q_date_from" />
			</th>
			<th>
				To:
				<input type="datetime" id="q_date_to" />
			</th>
			<th>
			<select id="q_time_condition">
				<option value="all">all</option>
				<option value="=">equal to</option>
				<option value="!=">not equal to</option>
				<option value=">">greather then</option>
				<option value="<">less then</option>
				<option value=">=">equal or greather then</option>
				<option value="<=">equal or less then</option>
			</select>
			<input type="text" id="q_time_value" />
		</th>
			<th>
				<select id="q_controller">
					<option value="all">all</option>
					<?php
					foreach($list_controllers as $value)
					{
						echo '<option value="'.$value.'">'.$value.'</option>';
					}
					?>
				</select>
			</th>
			<th>
				<select id="q_action">
					<option value="all">all</option>
					<?php
					foreach($list_actions as $value)
					{
						echo '<option value="'.$value.'">'.$value.'</option>';
					}
					?>
				</select>
			</th>
		</tr>
</table>
</form>
<hr/>
<div id="query_results"></div>
<script>
/* bind jquery-ui calendar plugin to date selection boxes*/
$( function() {
	$( "#q_date_from" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
} );
$( function() {
	$( "#q_date_to" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
} );

/* get values from the inputs and pass them via ajax to the query building scrips; display results in the div */
$("#submit_custom_query").on("click", function () {
	var method = $("#q_method").val();
	var type = $("#q_type").val();
	var date_from = $("#q_date_from").val();
	var date_to = $("#q_date_to").val();
	var execution_rule = $("#q_time_condition").val();
	var execution = $("#q_time_value").val();
	var controller = $("#q_controller").val();
	var action = $("#q_action").val();
	$.ajax({
							type: "POST",
							url: "includes/run_custom_query.php", // url to the controller handling the request
							data: {
											fld_method: method,
											fld_type: type,
											fld_date_from: date_from,
											fld_date_to:date_to,
											fld_execution_rule:execution_rule,
											fld_execution:execution,
											fld_controller:controller,
											fld_action:action
										},
							dataType: "html",
							success: function (response)
							{
									$('#query_results').html(response);
							}
					});
});


</script>
